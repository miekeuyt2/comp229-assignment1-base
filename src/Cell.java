import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Cell  {

    //-------------Variables-----------//
    int x;
    int y;
    private Grid grid;
    Color thisColour = getColour();
    public static int grassHeight = 0;
   //----------------------------------//

    //Constructor for Cell
    public Cell(int x, int y) {
        this.x = x;
        this.y = y;


    }

    //---Method to return a colour--//
    public Color getColour() {
       Random rand = new Random();
       //Get random number to take away from Green value, maximum 170
        int k = rand.nextInt(170);
        //Green value = 255 - random number
         k = 255 -k;

      //Get random Red value, 1/15th the size of the Green value
        int i = rand.nextInt(k /15);
        //Get random Blue value, 1/15th the size of the Green value
        int j = rand.nextInt(k/15);

        //return new colour
        return new Color(i,k,j);
    }
    //------------------------------//

    //---------------------Paint method-------------------------//
    public void paint(Graphics g, Boolean highlighted) {
        //Draw the cells
        g.setColor(thisColour);
        g.fillRect(x, y, 35, 35);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, 35, 35);
        //
        //If mouse is hovered
        if (highlighted) {
            //draw highlighter
            g.setColor(Color.yellow);
            g.drawRect(x+1, y+1, 33, 33);
            //

            //grid null catch
            if (grid == null) {

            }
            //

            //Set grassHeight
            grassHeight = thisColour.getGreen()/50;
            //

        }
    }
    //-----------------end paint method------------------------//


    //method to check if cell contains target
    public boolean contains(Point target){
        if (target == null)
            return false;
        return target.x > x && target.x < x + 35 && target.y > y && target.y < y +35;
    }
}
