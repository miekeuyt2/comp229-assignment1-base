import java.awt.*;

public class Grid {


    //--------------/*Variables*/---------------------//
    private Cell[][] cells = new Cell[20][20];

    private int x;
    private int y;
    private int mx = 0;
    private int my = 0;
    private Point storedMouse = null;
    private int tooltipCount = 0;
    private int START_COUNT = 90;
    private int END_COUNT = 1000;
    //----------------------------------------------//


    //Grid Constructor
    public Grid(int x, int y) {
        this.x = x;
        this.y = y;

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                cells[i][j] = new Cell(x + j * 35, y + i * 35);
            }
        }
    }
    //


    //////////////------Paint------------------/////////////
    public void paint(Graphics g, Point mousePosition) {

        //Draw the cells
        for (int y = 0; y < 20; ++y) { //for all of y
            for (int x = 0; x < 20; ++x) { // for all of x
                Cell thisCell = cells[x][y];
                //Draw cell
                thisCell.paint(g, thisCell.contains(mousePosition));

            }
        }
        //////////////

        //MousePosition null catch
        if (mousePosition == null) {
            //do nothing
        }

        else{
            //If mouse hasn't moved
            if (hasMouseMoved(mousePosition, storedMouse) == false) {
                //if tooltip is between START_COUNT and END_COUNT, and the grid contains the mouse
                if (tooltipCount >= START_COUNT && tooltipCount <= END_COUNT && this.contains(mousePosition)) {
                    //Set tooltip position
                    mx = mousePosition.x;
                    my = mousePosition.y;

                    //Draw tooltip
                    drawTool(g);
                }

                //Add to counter
                tooltipCount++;
            }

            //If the mouse has moved,
            else {
                //Reset counter
                tooltipCount = 0;
            }


            //then set storedMouse position
            storedMouse = mousePosition;
        }

    }
    ///////////////////////////////////////////////////////


    //Draw tooltip method
    public void drawTool(Graphics g){
        //g is null catch
        if(g == null){

        }

        else {
            //draw tooltip yellow box
            g.setColor(Color.yellow);
            g.fillRect(mx + 10, my + 5, 48, 15);
            //draw tooltip text
            g.setColor(Color.black);
            g.drawString("grass: " + Cell.grassHeight, mx + 10, my + 16);
        }
    }


    //Contains method to check if it's in the grid
    public boolean contains(Point target){
        //Use gridSize (20 cells, width 35, height 35)
        int gridSize = 35*20;
        if (target == null)
            return false;
        return target.x > x && target.x < x + (gridSize) && target.y > y && target.y < y + (gridSize);
    }


    //Method to check if the mouse has moved
    boolean hasMouseMoved(Point mouse, Point mouseNow){
        if(mouse.equals(mouseNow)){
            return false;
        }
        return true;
    }

}



